var sensor = require("node-dht-sensor");
const express = require('express')
const http = require('http')
const socketIO = require('socket.io')
/*
sensor.read(11, 4, function(err, temperature, humidity) {

	console.log("--",err);
  if (!err) {
    console.log(`temp: ${temperature}°C, humidity: ${humidity}%`);
  }
});

*/

// our localhost port
const port = 4000;
const app = express();
// our server instance
const server = http.createServer(app)
// This creates our socket using the instance of the server
const io = socketIO(server)
// This is what the socket.io syntax is like, we will work this later
io.on('connection', socket => {
  console.log('New client connected')
  
  // just like on the client side, we have a socket.on method that takes a callback function
  socket.on('change color', (color) => {
    // once we get a 'change color' event from one of our clients, we will send it to the rest of the clients
    // we make use of the socket.emit method again with the argument given to use from the callback function above
    console.log('Color Changed to: ', color)
    io.sockets.emit('change color', color)
  })
  
  // disconnect is fired when a client leaves the server
  socket.on('disconnect', () => {
    console.log('user disconnected')
  })
})

server.listen(port, () => console.log(`Listening on port ${port}`))
async function exec() {
  try {
    const res = await sensor.read(11, 4);
    console.log(
      `temp: ${res.temperature.toFixed(1)}°C, ` +
        `humidity: ${res.humidity.toFixed(1)}%`
    );
    io.sockets.emit('ontemperature', res)
  } catch (err) {
    console.error("Failed to read sensor data:", err);
  }
}

exec();
setInterval(()=>{exec()}, 10000)