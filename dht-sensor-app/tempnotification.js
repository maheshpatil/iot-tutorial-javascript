var sensor = require("node-dht-sensor");
const express = require('express')
const http = require('http')
const socketIO = require('socket.io')

var Gpio = require('pigpio').Gpio, //include pigpio to interact with the GPIO
    ledRed = new Gpio(6, { mode: Gpio.OUTPUT }), //use GPIO pin 4 as output for RED
    ledGreen = new Gpio(17, { mode: Gpio.OUTPUT }), //use GPIO pin 17 as output for GREEN
    ledBlue = new Gpio(27, { mode: Gpio.OUTPUT }), //use GPIO pin 27 as output for BLUE
    redRGB = 255, //set starting value of RED variable to off (255 for common anode)
    greenRGB = 255, //set starting value of GREEN variable to off (255 for common anode)
    blueRGB = 255; //set starting value of BLUE variable to off (255 for common anode)

//RESET RGB LED
ledRed.digitalWrite(1); // Turn RED LED off
ledGreen.digitalWrite(1); // Turn GREEN LED off
ledBlue.digitalWrite(1); // Turn BLUE LED off

// our localhost port
const port = 4000;
const app = express();
// our server instance
const server = http.createServer(app)
    // This creates our socket using the instance of the server
const io = socketIO(server)
    // This is what the socket.io syntax is like, we will work this later
process.on('SIGINT', function() { //on ctrl+c
    ledRed.digitalWrite(1); // Turn RED LED off
    ledGreen.digitalWrite(1); // Turn GREEN LED off
    ledBlue.digitalWrite(1); // Turn BLUE LED off
    process.exit(); //exit completely
});
io.on('connection', socket => {
    console.log('New client connected')

    // just like on the client side, we have a socket.on method that takes a callback function
    socket.on('ledcolor', (color) => {
        // once we get a 'change color' event from one of our clients, we will send it to the rest of the clients
        // we make use of the socket.emit method again with the argument given to use from the callback function above
        console.log('Device Changed to: ', color)
        switch (color) {
            case 'red':
                ledRed.digitalWrite(0); // Turn RED LED ON
                ledGreen.digitalWrite(1); // Turn GREEN LED off
                ledBlue.digitalWrite(1); // Turn BLUE LED off
                break;
            case 'green':
                ledRed.digitalWrite(1); // Turn RED LED OFF
                ledGreen.digitalWrite(0); // Turn GREEN LED ON
                ledBlue.digitalWrite(1); // Turn BLUE LED off
                break;
            case 'blue':
                ledRed.digitalWrite(1); // Turn RED LED off
                ledGreen.digitalWrite(1); // Turn GREEN LED off
                ledBlue.digitalWrite(0); // Turn BLUE LED ON
                break;
            case 'off':
                ledRed.digitalWrite(1); // Turn RED LED off
                ledGreen.digitalWrite(1); // Turn GREEN LED off
                ledBlue.digitalWrite(1); // Turn BLUE LED off
                break;
            default:
                ledRed.digitalWrite(0); // Turn RED LED off

        }
        io.sockets.emit('ledcolor', color);

    })

    //Get temperature
    socket.on('getTemp', (color) => {
        exec();
    });

    // disconnect is fired when a client leaves the server
    socket.on('disconnect', () => {
        console.log('user disconnected')
    })
})

server.listen(port, () => console.log(`Listening on port ${port}`))
async function exec() {
    try {
        const res = await sensor.read(11, 4);
        console.log(
            `temp: ${res.temperature.toFixed(1)}°C, ` +
            `humidity: ${res.humidity.toFixed(1)}%`
        );
        io.sockets.emit('ontemperature', res)

    } catch (err) {
        console.error("Failed to read sensor data:", err);
    }
}

exec();
//setInterval(()=>{exec()}, 10000)