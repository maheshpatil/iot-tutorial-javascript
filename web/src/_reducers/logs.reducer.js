import { logsConstants } from '../_constants';

export function logs(state = {}, action) {
  switch (action.type) {
    case logsConstants.GETLOGS_REQUEST:
      return {
        loading: true
      };
    case logsConstants.GETLOGS_SUCCESS:
      return {
        items: action.logs
      };
    case logsConstants.GETLOGS_FAILURE:
      return { 
        error: action.error
      };
    default:
      return state
  }
}