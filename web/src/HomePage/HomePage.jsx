import React from 'react';
import { Container, Row, Col, Button,Table } from 'reactstrap';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import socketIOClient from 'socket.io-client';

import { userActions,logsActions,alertActions,addNotification, deviceActions } from '../_actions';
import { NotificationContainer } from '../NotificationContainer';
class HomePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
           // endpoint: "http://192.168.43.114:4000",
            endpoint: "http://192.168.43.114:4000",
            deviceStatus: 'red',
            temperature: "NA",
            humidity: "NA",
          };
        const { dispatch } = this.props;
    }
      // sending sockets
    getTemperature = () => {
        const socket = socketIOClient(this.state.endpoint);
        console.log('----called getTemperature----');
        socket.emit('getTemp'); // change 'red' to this.state.cooler
    }
    // adding the function
    changeDeviceState = (color) => {
        //this.setState({ deviceStatus:color });
        //this.props.dispatch(deviceActions.changeDeviceStatus(color));  
        const socket = socketIOClient(this.state.endpoint);
        console.log('called changeDeviceState--',color);
        socket.emit('ledcolor', color) // change 'red' to this.state.cooler
    }
    componentDidMount() {
        this.props.dispatch(userActions.getAll());
        this.props.dispatch(logsActions.getLogs(1));
        const socket = socketIOClient(this.state.endpoint);
        //setInterval(this.setTemperature(), 10000);
        socket.on('ontemperature', (result) => {
            console.log("received ",result);
          //  this.props.dispatch(alertActions.success( `Temperature is: ${temp.temperature}` ));
            this.props.dispatch(addNotification('happy days', 'success'));
            this.setState({temperature: result.temperature});
            this.setState({humidity: result.humidity});
        });
    }
    handleDeleteUser(id) {
        return (e) => this.props.dispatch(userActions.delete(id));
    }

    render() {
        const { user, users, logs } = this.props;
        const  temperature  = this.state.temperature;
        const  deviceStatus  = this.state.deviceStatus;
        const  humidity = this.state.humidity;
        return (
            <Container>
            <Row>
                <Col xs="12">
                <h1>Hi {user.firstName}!</h1>
                <p>You're logged in Smart IoT Manager!!</p>
                <div>
                    <Button onClick={() => this.getTemperature() }>Get Temperature</Button>&nbsp;
                    <Button onClick={() => this.changeDeviceState('red')}>RED Device</Button>&nbsp;
                    <Button onClick={() => this.changeDeviceState('green')}>GREEN Device</Button>&nbsp;
                    <Button onClick={() => this.changeDeviceState('blue')}>BLUE Device</Button>&nbsp;
                    <Button onClick={() => this.changeDeviceState('off')}>OFF Device</Button>&nbsp;
                    <Button onClick={() => this.changeDeviceState('on')}>ON Device</Button>&nbsp;
                    <div>
                        {
                        temperature ? <p>The temperature in Floor is: {temperature} °C, Humidity: {humidity} %  </p>  : <p>Loading...</p>
                        }
                        </div>   
                </div>
                <p>
                    <Link to="/login">Logout</Link>
                </p>
                <NotificationContainer />
                </Col>
            </Row>
          </Container>

        );
    }
}

function mapStateToProps(state) {
    const { users, authentication,logs } = state;
    const { user } = authentication;
    return {
        user,
        users,
        logs
    };
}

const connectedHomePage = connect(mapStateToProps)(HomePage);
export { connectedHomePage as HomePage };