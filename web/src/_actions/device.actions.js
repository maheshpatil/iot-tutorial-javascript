import { deviceConstants } from '../_constants';
import { deviceService } from '../_services';
import { alertActions } from '.';
import { history } from '../_helpers';

export const deviceActions = {
    changeDeviceStatus
};

function changeDeviceStatus(status) {
    return dispatch => {
        dispatch(request({ status }));

        deviceService.changeDeviceStatus(status)
            .then(
                result => { 
                    dispatch(success(result));
                    history.push('/');
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(status) { return { type: deviceConstants.CHANGESTATUS_DEVICE, status } }
    function success(result) { return { type: deviceConstants.CHANGESTATUS_DEVICE, result } }
    function failure(error) { return { type: deviceConstants.CHANGESTATUS_DEVICE, error } }
}