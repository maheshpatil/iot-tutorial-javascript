import { logsConstants } from '../_constants';
import { logsService } from '../_services';

export const logsActions = {
    getLogs,
};

function getLogs(days) {
    return dispatch => {
        dispatch(request());

        logsService.getLogs(days)
            .then(
                logs => dispatch(success(logs)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: logsConstants.GETLOGS_REQUEST } }
    function success(logs) { return { type: logsConstants.GETLOGS_SUCCESS, logs } }
    function failure(error) { return { type: logsConstants.GETLOGS_FAILURE, error } }
}
