import { notificationConstants } from '../_constants';
/*
const addNotification = (id, requestStatus) => {
   return { type: notificationConstants.ADD_NOTIFICATION, payload: {id: id, data: requestStatus}}
 }

const removeNotification = (id) => {
   return { type: notificationConstants.REMOVE_NOTIFICATION, payload: id }
 }

let nextNotificationId = 0;
export const showNotification = (requestStatus) => {
   return function (dispatch) {
     const id = nextNotificationId++;
     dispatch(addNotification(id, requestStatus));
     dispatch(removeNotification(id));
   }
}*/

export function addNotification(message, level) {
    return  {
      type: notificationConstants.ADD_NOTIFICATION,
      message,
      level
    };
  }
  
  /*
export const notificationActions = {
    addNotification,
};

function addNotification(message, level) {
    return dispatch => {
        dispatch(request());
        dispatch(success({
            type: notificationConstants.ADD_NOTIFICATION,
            message,
            level
          }));
    };

    function request() { return { type: notificationConstants.ADD_NOTIFICATION } }
    function success(notification) { return { type: notificationConstants.ADD_NOTIFICATION, notification } }
    function failure(error) { return { type: notificationConstants.ADD_NOTIFICATION, error } }
}
*/