import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import NotificationSystem from 'react-notification-system';
import { addNotification } from '../_actions';

class NotificationContainer  extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.notificationSystem = this.refs.notificationSystem;
      }
    
      componentWillReceiveProps(newProps) {
        const { message, level } = newProps.notification;
        this.notificationSystem.addNotification({
          message,
          level
        });
      }

    render() {
        return (<NotificationSystem ref="notificationSystem" />);
    }
}

function mapStateToProps(state) {
    return {notification: state.notification};
}

function mapDispatchToProps(dispatch) {
    return {
      actions: bindActionCreators({
        addNotification
      }, dispatch)
    };
  }

const connectedNotificationContainer = connect(mapStateToProps,mapDispatchToProps)(NotificationContainer);
export { connectedNotificationContainer as NotificationContainer };